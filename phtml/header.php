<?php
require_once dirname(__FILE__) . '/../includes/config.php';
require_once APPLICATION_ROOT . '/includes/session.php';
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title><?php echo APPLICATION_NAME ?></title>

        <link href="<?php echo APPLICATION_BASE_URL ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo APPLICATION_BASE_URL ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="<?php echo APPLICATION_BASE_URL ?>/theme.css" rel="stylesheet">

        <!--[if lt IE 9]>
          <script src="<?php echo APPLICATION_BASE_URL ?>/assets/js/html5shiv.js"></script>
          <script src="<?php echo APPLICATION_BASE_URL ?>/assets/js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <?php include dirname(__FILE__) . '/navigation.php'; ?>

        <!-- Content -->
        <div class="container theme-showcase">
            <h1><?php echo defined('PAGE_TITLE') ? PAGE_TITLE : ucwords(substr(basename($_SERVER['SCRIPT_NAME']), 0, -4)) ?>
                <?php if (defined('PAGE_DESCRIPTION')): ?><small><?php echo PAGE_DESCRIPTION ?></small><?php endif; ?></h1>
            <hr />
            <?php foreach (session_get_error_messages(true) as $message): ?>
                <div class="alert alert-danger">
                    <?php echo htmlentities($message, ENT_COMPAT | ENT_HTML5, 'UTF-8'); ?>
                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                </div>
            <?php endforeach; ?>
            <?php foreach (session_get_success_messages(true) as $message):
                ?>
                <div class="alert alert-success">
                    <?php echo htmlentities($message, ENT_COMPAT | ENT_HTML5, 'UTF-8'); ?>
                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                </div>
            <?php endforeach; ?>
