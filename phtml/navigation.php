<?php
require_once APPLICATION_ROOT . '/includes/session.php';
require_once APPLICATION_ROOT . '/includes/user.php';
?>
<!-- Navigation -->
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#" title="<?php echo sprintf('%s (v%s)', APPLICATION_NAME, APPLICATION_VERSION) ?>"><?php echo APPLICATION_NAME ?></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li<?php if (defined('PAGE_ID') && PAGE_ID == 'BOOKS'): ?> class="active"<?php endif; ?>><a href="<?php echo APPLICATION_CONTROLLERS_URL . '/books/list.php' ?>">Books</a></li>
                <li<?php if (defined('PAGE_ID') && PAGE_ID == 'AUTHORS'): ?> class="active"<?php endif; ?>><a href="<?php echo APPLICATION_CONTROLLERS_URL . '/authors/list.php' ?>">Authors</a></li>
            </ul>
            <ul class="nav navbar-nav pull-right">
                <?php if (session_has_user()): ?>
                    <li><a>Welcome, <?php echo htmlentities(session_get_user_username(), ENT_COMPAT | ENT_HTML5, 'UTF-8') ?>!</a></li>
                    <li><a href="<?php echo APPLICATION_BASE_URL . '/logout.php' ?>">Logout</a></li>
                <?php else: ?>
                    <li><a href="<?php echo APPLICATION_BASE_URL . '/login.php' ?>">Login</a></li>
                    <li><a href="<?php echo APPLICATION_BASE_URL . '/register.php' ?>">Register</a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</div>
