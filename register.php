<?php
require_once 'includes/config.php';
require_once APPLICATION_ROOT . '/includes/session.php';
require_once APPLICATION_ROOT . '/includes/validate.php';
require_once APPLICATION_ROOT . '/includes/user.php';

$errors = array();
if ($_POST) {

    $username = trim($_POST['username']);
    $password = trim($_POST['password']);
    $password_confirm = trim($_POST['password_confirm']);

    if (($username_error = validate_length($username, 5, 25)) !== VALIDATION_OK) {
        $errors['username'] = $username_error;
    } elseif (user_username_exists($username)) {
        $errors['username'] = 'User with this username already exists';
    }

    if (($password_error = validate_length($password, 5, 30)) !== VALIDATION_OK) {
        $errors['password'] = $password_error;
    } elseif ($password !== $password_confirm) {
        $errors['password_confirm'] = 'Passwords does not match.';
    }



    if (empty($errors)) {
        $user = array(
            'type' => USER_TYPE_USER,
            'username' => $username,
            'password' => $password,
        );

        if (user_create($user)) {
            session_add_success_messages('Successful registration. You can now login.');
            header('Location: login.php');
            exit;
        } else {
            session_add_error_messages('An error occured while trying to create user account.');
        }
    }
}

include APPLICATION_ROOT . '/phtml/header.php';
?>

<div class="row">
    <div class="panel panel-default col-md-4 col-md-offset-4">
        <div class="panel-body">
            <h3>Register</h3>
            <hr />
            <form role="form" method="post">
                <div class="form-group<?php if (isset($errors['username'])): ?> has-error<?php endif; ?>">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username" maxlength="25" placeholder="Username" value="<?php echo $_POST ? $username : '' ?>">
                    <?php if (isset($errors['username'])): ?><p class="help-block has-error"><?php echo $errors['username'] ?></p><?php endif; ?>
                </div>
                <div class="form-group<?php if (isset($errors['password'])): ?> has-error<?php endif; ?>">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" maxlength="25" placeholder="Password">
                    <?php if (isset($errors['password'])): ?><p class="help-block has-error"><?php echo $errors['password'] ?></p><?php endif; ?>
                </div>
                <div class="form-group<?php if (isset($errors['password_confirm'])): ?> has-error<?php endif; ?>">
                    <label for="password_confirm">Password</label>
                    <input type="password" class="form-control" id="password_confirm" name="password_confirm" maxlength="25" placeholder="Confirm Password">
                    <?php if (isset($errors['password_confirm'])): ?><p class="help-block has-error"><?php echo $errors['password_confirm'] ?></p><?php endif; ?>
                </div>
                <button type="submit" class="btn btn-primary">Register</button>
                or <a href="login.php">Login</a>
            </form>
        </div>
    </div>
</div>

<?php
include APPLICATION_ROOT . '/phtml/footer.php';
