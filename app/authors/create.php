<?php

defined('AUTHOR_CREATE') || define('AUTHOR_CREATE', true);

defined('PAGE_ID') || define('PAGE_ID', 'AUTHORS');
defined('PAGE_TITLE') || define('PAGE_TITLE', 'Authors');
defined('PAGE_DESCRIPTION') || define('PAGE_DESCRIPTION', 'Create new author');

include 'edit.php';
