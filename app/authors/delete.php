<?php

require_once dirname(__FILE__) . '/../../includes/config.php';
require_once APPLICATION_ROOT . '/includes/session.php';
require_once APPLICATION_ROOT . '/includes/author.php';

if (!isset($_GET['author'])) {
    header('Location: list.php');
    exit;
}

$author_id = (int) $_GET['author'];

if (author_delete($author_id)) {
    session_add_success_messages('Author deleted successfully.');
} else {
    session_add_error_messages('Requested author not found.');
}

header('Location: list.php');
exit;
