<?php
require_once dirname(__FILE__) . '/../../includes/config.php';
require_once APPLICATION_ROOT . '/includes/author.php';

defined('PAGE_ID') || define('PAGE_ID', 'AUTHORS');
defined('PAGE_TITLE') || define('PAGE_TITLE', 'Authors');
defined('PAGE_DESCRIPTION') || define('PAGE_DESCRIPTION', 'List authors');

include APPLICATION_ROOT . '/phtml/header.php';
?>

<div class="navbar navbar-default" role="navigation">
    <div class="navbar-collapse">
        <a class="btn btn-primary navbar-btn pull-left" href="create.php" title="New message"><span class="glyphicon glyphicon-plus-sign"></span> New author</a>
    </div>
</div>

<table class="table table-condensed table-hover table-responsive">
    <thead>
        <tr>
            <th>Author name</th>
            <th class="col-md-1">Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach (author_find_all() as $author): ?>
            <tr>
                <td><?php echo htmlentities($author['author_name'], ENT_COMPAT | ENT_HTML5, 'UTF-8'); ?></td>
                <td class="text-right">
                    <a class="btn btn-default btn-xs" href="../books/list.php?author=<?php echo $author['author_id'] ?>" title="View books"><span class="glyphicon glyphicon-book"></span></a>
                    <a class="btn btn-default btn-xs" href="edit.php?author=<?php echo $author['author_id'] ?>" title="Edit author"><span class="glyphicon glyphicon-pencil"></span></a>
                    <a class="btn btn-default btn-xs" onclick="return confirm('Are you sure that you want to delete this author?');" href="delete.php?author=<?php echo $author['author_id'] ?>" title="Delete author"><span class="glyphicon glyphicon-trash"></span></a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php
include APPLICATION_ROOT . '/phtml/footer.php';
