<?php
require_once dirname(__FILE__) . '/../../includes/config.php';
require_once APPLICATION_ROOT . '/includes/session.php';
require_once APPLICATION_ROOT . '/includes/validate.php';
require_once APPLICATION_ROOT . '/includes/author.php';

defined('AUTHOR_CREATE') || define('AUTHOR_CREATE', false);

defined('PAGE_ID') || define('PAGE_ID', 'AUTHORS');
defined('PAGE_TITLE') || define('PAGE_TITLE', 'Authors');
defined('PAGE_DESCRIPTION') || define('PAGE_DESCRIPTION', 'Edit author');

$author_id = 0;
$author_name = '';

if (!AUTHOR_CREATE) {
    if (!isset($_GET['author']) || !($author = author_load($_GET['author']))) {
        session_add_error_messages('Requested author cannot be found.');
        header('Location: list.php');
        exit;
    }

    $author_id = $author['author_id'];
    $author_name = $author['author_name'];
}

$errors = array();
if ($_POST) {

    $author_name = trim($_POST['author_name']);

    if (($author_name_error = validate_length($author_name, 3, 250)) !== VALIDATION_OK) {
        $errors['author_name'] = $author_name_error;
    } elseif (author_name_exists($author_name, $author_id)) {
        $errors['author_name'] = 'Author with this name already exists';
    }

    if (empty($errors)) {
        $data = array(
            'author_name' => $author_name,
        );

        if (AUTHOR_CREATE) {

            if (author_create($data)) {
                session_add_success_messages('Author created successfully!');
                header('Location: list.php');
                exit;
            } else {
                session_add_error_messages('An error occured while trying to create the author.');
            }
        } else {
            if (author_update($author_id, $data)) {
                session_add_success_messages('Author updated successfully!');
                header('Location: list.php');
                exit;
            } else {
                session_add_error_messages('An error occured while trying to update the author.');
            }
        }
    }
}

include APPLICATION_ROOT . '/phtml/header.php';
?>

<div class="row">
    <div class="panel panel-default col-md-6 col-md-offset-3">
        <div class="panel-body">
            <form role="form" method="post">
                <div class="form-group<?php if (isset($errors['author_name'])): ?> has-error<?php endif; ?>">
                    <label for="author_name">Author name</label>
                    <input type="text" class="form-control" id="author_name" name="author_name" maxlength="250" placeholder="Author name" value="<?php echo htmlspecialchars($author_name, ENT_QUOTES, 'UTF-8') ?>">
                    <?php if (isset($errors['author_name'])): ?><p class="help-block has-error"><?php echo $errors['author_name'] ?></p><?php endif; ?>
                </div>
                <button type="submit" class="btn btn-primary">Save author</button>
                or <a href="list.php">Cancel</a>
            </form>
        </div>
    </div>
</div>

<?php
include APPLICATION_ROOT . '/phtml/footer.php';
