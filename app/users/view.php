<?php
require_once dirname(__FILE__) . '/../../includes/config.php';
require_once APPLICATION_ROOT . '/includes/comment.php';
require_once APPLICATION_ROOT . '/includes/user.php';

if (!isset($_GET['user'])) {
    header('Location: ' . APPLICATION_BASE_URL);
    exit;
}

$user = user_load((int) $_GET['user']);

if (!$user) {
    session_add_error_messages('Requested user not found.');
    header('Location: ' . APPLICATION_BASE_URL);
    exit;
}

$comments = comment_find_all_by_user($user['id']);

defined('PAGE_ID') || define('PAGE_ID', 'BOOKS');
defined('PAGE_TITLE') || define('PAGE_TITLE', 'Users');
defined('PAGE_DESCRIPTION') || define('PAGE_DESCRIPTION', 'View user <strong>' . htmlentities($user['username'], ENT_COMPAT | ENT_HTML5, 'UTF-8') . '</strong>');

include APPLICATION_ROOT . '/phtml/header.php';
?>

<div class="media">
    <a class="pull-left" href="#">
        <img class="media-object" src="http://lorempixel.com/100/100/" alt="">
    </a>
    <div class="media-body">
        <dl class="dl-horizontal" style="margin-top: 0;">
            <dt>Username:</dt>
            <dd><?php echo htmlentities($user['username'], ENT_COMPAT | ENT_HTML5, 'UTF-8') ?></dd>
            <dt>Type:</dt>
            <dd><?php
                switch ($user['type']) {
                    case USER_TYPE_ADMIN:
                        echo 'Administrator';
                        break;
                    case USER_TYPE_USER:
                        echo 'User';
                        break;
                    default:
                        echo 'Unknown';
                }
                ?></dd>
            <dt>Comments</dt>
            <dd><?php echo count($comments) ?></dd>
        </dl>
    </div>
</div>

<hr />

<h3>Comments (<?php echo count($comments) ?>)</h3>

<ul class="media-list">
    <?php foreach ($comments as $comment): ?>
        <li class="media" id="comment<?php echo $comment['id'] ?>">
            <hr />
            <a class="pull-left" href="#">
                <img class="media-object" src="http://lorempixel.com/64/64/" alt="">
            </a>
            <div class="media-body">
                <?php echo htmlentities($comment['message'], ENT_COMPAT | ENT_HTML5, 'UTF-8') ?>
                <hr />
                <p class="help-block"><small>Posted on <a href="<?php echo APPLICATION_BASE_URL ?>/app/books/view.php?book=<?php echo $comment['book']['book_id'] ?>"><?php echo htmlentities($comment['book']['book_title'], ENT_COMPAT | ENT_HTML5, 'UTF-8') ?></a> at <span><?php echo $comment['create_dt'] ?></span></small></p>
            </div>
        </li>
    <?php endforeach; ?>
</ul>

<?php
include APPLICATION_ROOT . '/phtml/footer.php';
