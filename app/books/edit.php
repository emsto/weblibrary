<?php
require_once dirname(__FILE__) . '/../../includes/config.php';
require_once APPLICATION_ROOT . '/includes/session.php';
require_once APPLICATION_ROOT . '/includes/validate.php';
require_once APPLICATION_ROOT . '/includes/author.php';
require_once APPLICATION_ROOT . '/includes/book.php';

defined('BOOK_CREATE') || define('BOOK_CREATE', false);

defined('PAGE_ID') || define('PAGE_ID', 'BOOKS');
defined('PAGE_TITLE') || define('PAGE_TITLE', 'Books');
defined('PAGE_DESCRIPTION') || define('PAGE_DESCRIPTION', 'Edit book');

$book_id = 0;
$book_title = '';
$book_authors = array();


$authors = author_find_all();

if (!BOOK_CREATE) {
    if (!isset($_GET['book']) || !($book = book_load($_GET['book']))) {
        session_add_error_messages('Requested book cannot be found.');
        header('Location: list.php');
        exit;
    }

    $book_id = $book['book_id'];
    $book_title = $book['book_title'];
    $book_authors = $book['authors'];
}

$errors = array();

if ($_POST) {

    $book_title = trim($_POST['book_title']);
    $book_authors = array();

    if (isset($_POST['authors'])) {
        foreach ($_POST['authors'] as $author_id) {
            $author_id = (int) $author_id;
            if (array_key_exists($author_id, $authors)) {
                $book_authors[$author_id] = $authors[$author_id];
            }
        }
    }

    if (($book_title_error = validate_length($book_title, 3, 250)) !== VALIDATION_OK) {
        $errors['book_title'] = $book_title_error;
    } elseif (book_title_exists($book_title, $book_id)) {
        $errors['book_title'] = 'Book with this title already exists';
    }

    if (empty($errors)) {
        $data = array(
            'book_title' => $book_title,
            'authors' => $book_authors,
        );

        if (BOOK_CREATE) {
            if (book_create($data)) {
                session_add_success_messages('Book created successfully!');
                header('Location: list.php');
                exit;
            } else {
                session_add_error_messages('An error occured while trying to create the book.');
            }
        } else {
            if (book_update($book_id, $data)) {
                session_add_success_messages('Book updated successfully!');
                header('Location: list.php');
                exit;
            } else {
                session_add_error_messages('An error occured while trying to update the book.');
            }
        }
    }
}

include APPLICATION_ROOT . '/phtml/header.php';
?>

<div class="row">
    <div class="panel panel-default col-md-6 col-md-offset-3">
        <div class="panel-body">
            <form role="form" method="post">
                <div class="form-group<?php if (isset($errors['book_title'])): ?> has-error<?php endif; ?>">
                    <label for="book_title">Book title</label>
                    <input type="text" class="form-control" id="book_title" name="book_title" maxlength="250" placeholder="Book title" value="<?php echo htmlspecialchars($book_title, ENT_QUOTES, 'UTF-8') ?>">
                    <?php if (isset($errors['book_title'])): ?><p class="help-block has-error"><?php echo $errors['book_title'] ?></p><?php endif; ?>
                </div>
                <div class="form-group<?php if (isset($errors['authors'])): ?> has-error<?php endif; ?>">
                    <label for="authors">Authors</label>
                    <select class="form-control" id="authors" name="authors[]" multiple="multiple">
                        <?php foreach ($authors as $author): ?>
                            <option value="<?php echo $author['author_id'] ?>"<?php if (array_key_exists($author['author_id'], $book_authors)): ?> selected="selected"<?php endif; ?>><?php echo htmlentities($author['author_name'], ENT_COMPAT | ENT_HTML5, 'UTF-8') ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?php if (isset($errors['authors'])): ?><p class="help-block has-error"><?php echo $errors['authors'] ?></p><?php endif; ?>
                </div>
                <button type="submit" class="btn btn-primary">Save book</button>
                or <a href="list.php">Cancel</a>
            </form>
        </div>
    </div>
</div>

<?php
include APPLICATION_ROOT . '/phtml/footer.php';
