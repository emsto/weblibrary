<?php

defined('BOOK_CREATE') || define('BOOK_CREATE', true);

defined('PAGE_ID') || define('PAGE_ID', 'BOOKS');
defined('PAGE_TITLE') || define('PAGE_TITLE', 'Books');
defined('PAGE_DESCRIPTION') || define('PAGE_DESCRIPTION', 'Create new book');

include 'edit.php';
