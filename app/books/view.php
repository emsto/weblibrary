<?php
require_once dirname(__FILE__) . '/../../includes/config.php';
require_once APPLICATION_ROOT . '/includes/session.php';
require_once APPLICATION_ROOT . '/includes/book.php';
require_once APPLICATION_ROOT . '/includes/comment.php';
require_once APPLICATION_ROOT . '/includes/validate.php';

if (!isset($_GET['book'])) {
    header('Location: list.php');
    exit;
}

$book = book_load((int) $_GET['book']);

if (!$book) {
    session_add_error_messages('Requested book not found.');
    header('Location: list.php');
    exit;
}

$comments = comment_find_all_by_book($book['book_id']);
$comment_message = '';

$errors = array();
if (session_has_user() && $_POST) {

    $comment_message = trim($_POST['message']);

    if (($message_error = validate_length($comment_message, 1, 250)) !== VALIDATION_OK) {
        $errors['message'] = $message_error;
    }

    if (empty($errors)) {
        $comment = array(
            'book_id' => $book['book_id'],
            'user_id' => session_get_user_id(),
            'message' => $comment_message,
            'create_dt' => date('Y-m-d H:i:s')
        );

        if (comment_create($comment)) {
            session_add_success_messages('Comment posted successfully!');
            header('Location: view.php?book=' . $book['book_id']);
            exit;
        } else {
            session_add_error_messages('An error occured while trying to post the comment.');
        }
    }
}

defined('PAGE_ID') || define('PAGE_ID', 'BOOKS');
defined('PAGE_TITLE') || define('PAGE_TITLE', 'Books');
defined('PAGE_DESCRIPTION') || define('PAGE_DESCRIPTION', 'View book <strong>' . htmlentities($book['book_title'], ENT_COMPAT | ENT_HTML5, 'UTF-8') . '</strong>');

include APPLICATION_ROOT . '/phtml/header.php';
?>

<div class="media">
    <a class="pull-left" href="#">
        <img class="media-object" src="http://lorempixel.com/100/120/" alt="...">
    </a>
    <div class="media-body">
        <h3 class="media-heading"><?php echo htmlentities($book['book_title'], ENT_COMPAT | ENT_HTML5, 'UTF-8') ?></h3>
        <?php foreach ($book['authors'] as $author): ?>
            <a href="list.php?author=<?php echo $author['author_id'] ?>"><?php echo $author['author_name'] ?></a>
        <?php endforeach; ?>
    </div>
</div>

<hr />

<h3>Comments (<?php echo count($comments) ?>)</h3>

<ul class="media-list">
    <?php foreach ($comments as $comment): ?>
        <li class="media" id="comment<?php echo $comment['id'] ?>">
            <hr />
            <a class="pull-left" href="#">
                <img class="media-object" src="http://lorempixel.com/64/64/" alt="">
            </a>
            <div class="media-body">
                <?php echo htmlentities($comment['message'], ENT_COMPAT | ENT_HTML5, 'UTF-8') ?>
                <hr />
                <p class="help-block"><small>Posted by <a href="<?php echo APPLICATION_BASE_URL ?>/app/users/view.php?user=<?php echo $comment['user']['id'] ?>"><?php echo htmlentities($comment['user']['username'], ENT_COMPAT | ENT_HTML5, 'UTF-8') ?></a> at <span><?php echo $comment['create_dt'] ?></span></small></p>
            </div>
        </li>
    <?php endforeach; ?>
</ul>

<?php if (session_has_user()): ?>
    <hr />
    <h3>Add comment</h3>
    <form role="form" method="post">
        <div class="form-group<?php if (isset($errors['message'])): ?> has-error<?php endif; ?>">
            <textarea class="form-control" id="message" name="message" maxlength="250" placeholder="Comment text"><?php echo htmlspecialchars($comment_message, ENT_QUOTES, 'UTF-8') ?></textarea>
            <?php if (isset($errors['message'])): ?><p class="help-block has-error"><?php echo $errors['message'] ?></p><?php endif; ?>
        </div>
        <button type="submit" class="btn btn-primary">Post comment</button>
    </form>
<?php endif; ?>
<?php
include APPLICATION_ROOT . '/phtml/footer.php';
