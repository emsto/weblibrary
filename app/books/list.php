<?php

require_once dirname(__FILE__) . '/../../includes/config.php';
require_once APPLICATION_ROOT . '/includes/author.php';
require_once APPLICATION_ROOT . '/includes/book.php';
require_once APPLICATION_ROOT . '/includes/session.php';


$author = null;
$author_id = null;

if (isset($_GET['author'])) {
    $author_id = (int) $_GET['author'];
    if (!($author = author_load($author_id))) {
        session_add_error_messages('Requested author not found.');
        header('Location: list.php');
        exit;
    }
}

$books = book_find_all($author_id);

defined('PAGE_ID') || define('PAGE_ID', 'BOOKS');
defined('PAGE_TITLE') || define('PAGE_TITLE', 'Books');
defined('PAGE_DESCRIPTION') || define('PAGE_DESCRIPTION', 'List books' . ($author ? ' by <strong>' . htmlentities($author['author_name'], ENT_COMPAT | ENT_HTML5, 'UTF-8') . '</strong>' : ''));

include APPLICATION_ROOT . '/phtml/header.php';
?>

<div class="navbar navbar-default" role="navigation">
    <div class="navbar-collapse">
        <a class="btn btn-primary navbar-btn pull-left" href="create.php" title="New message"><span class="glyphicon glyphicon-plus-sign"></span> New book</a>
        <!-- Search
        <form role="search" class="navbar-form navbar-right">
            <div class="form-group">
                <input type="text" placeholder="Search book" class="form-control">
            </div>
            <button class="btn btn-default" type="submit">Search</button>
        </form>
        -->
    </div>
</div>

<table class="table table-condensed table-hover table-responsive">
    <thead>
        <tr>
            <th>Title</th>
            <th>Authors</th>
            <th class="col-md-1">Comments</th>
            <th class="col-md-1">Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($books as $book): ?>
            <tr>
                <td><a href="view.php?book=<?php echo $book['book_id'] ?>" title="View book <?php echo htmlentities($book['book_title'], ENT_COMPAT | ENT_HTML5, 'UTF-8') ?>"><?php echo htmlentities($book['book_title'], ENT_COMPAT | ENT_HTML5, 'UTF-8'); ?></a></td>
                <td>
                    <?php foreach ($book['authors'] as $author): ?>
                        <a class="label label-default" href="list.php?author=<?php echo $author['author_id'] ?>"><?php echo $author['author_name'] ?></a>
                    <?php endforeach; ?>
                </td>
                <td><span class="badge"><?php echo $book['comments_count'] ?></span></td>
                <td class="text-right">
                    <a class="btn btn-default btn-xs" href="view.php?book=<?php echo $book['book_id'] ?>" title="View book"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a class="btn btn-default btn-xs" href="edit.php?book=<?php echo $book['book_id'] ?>" title="Edit book"><span class="glyphicon glyphicon-pencil"></span></a>
                    <a class="btn btn-default btn-xs" onclick="return confirm('Are you sure that you want to delete this book?');" href="delete.php?book=<?php echo $book['book_id'] ?>" title="Delete book"><span class="glyphicon glyphicon-trash"></span></a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php
include APPLICATION_ROOT . '/phtml/footer.php';
