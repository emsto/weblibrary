<?php

require_once dirname(__FILE__) . '/../../includes/config.php';
require_once APPLICATION_ROOT . '/includes/session.php';
require_once APPLICATION_ROOT . '/includes/book.php';

if (!isset($_GET['book'])) {
    header('Location: list.php');
    exit;
}

$book_id = (int) $_GET['book'];

if (book_delete($book_id)) {
    session_add_success_messages('Book deleted successfully.');
} else {
    session_add_error_messages('Requested book not found.');
}

header('Location: list.php');
exit;
