<?php

require_once 'config.php';

$mysqli_link = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE) or die('Cannot connect do database!');
mysqli_set_charset($mysqli_link, 'utf8');

function mysqli_get_link()
{
    global $mysqli_link;
    return $mysqli_link;
}
