<?php

/**
 * 
 * @param array $data
 * @return bool
 */
function comment_create(array $data)
{
    $sql = 'INSERT INTO ' . DB_TABLE_COMMENTS . ' (book_id, user_id, message, create_dt) VALUES (?, ? , ?, ?)';

    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, $sql);

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_bind_param($stmt, 'iiss', $data['book_id'], $data['user_id'], $data['message'], date('Y-m-d H:i:s'));
    mysqli_execute($stmt);

    return mysqli_stmt_affected_rows($stmt) > 0;
}

/**
 * 
 * @param int $book_id
 * @return array
 */
function comment_find_all_by_book($book_id)
{
    $book_id = (int) $book_id;

    $sql = 'SELECT c.id, u.id, u.username, c.message, c.create_dt FROM ' . DB_TABLE_COMMENTS . ' AS c';
    $sql .= ' JOIN users AS u ON u.id = c.user_id';
    $sql .= ' WHERE c.book_id = ?';

    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, $sql);

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_bind_param($stmt, 'i', $book_id);
    mysqli_stmt_execute($stmt);

    mysqli_stmt_bind_result($stmt, $comment_id, $user_id, $username, $message, $create_dt);

    $comments = array();
    while (mysqli_stmt_fetch($stmt)) {
        $comment = array(
            'id' => $comment_id,
            'user' => array(
                'id' => $user_id,
                'username' => $username,
            ),
            'message' => $message,
            'create_dt' => $create_dt,
        );

        $comments[] = $comment;
    }

    return $comments;
}

/**
 * 
 * @param int $user_id
 * @return array
 */
function comment_find_all_by_user($user_id)
{
    $user_id = (int) $user_id;

    $sql = 'SELECT c.id, c.message, c.create_dt, b.book_id, b.book_title FROM ' . DB_TABLE_COMMENTS . ' AS c';
    $sql .= ' JOIN books AS b ON b.book_id = c.book_id';
    $sql .= ' WHERE c.user_id = ?';

    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, $sql);

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_bind_param($stmt, 'i', $user_id);
    mysqli_stmt_execute($stmt);

    mysqli_stmt_bind_result($stmt, $comment_id, $comment_message, $commnet_create_dt, $book_id, $book_title);

    $comments = array();
    while (mysqli_stmt_fetch($stmt)) {
        $comment = array(
            'id' => $comment_id,
            'book' => array(
                'book_id' => $book_id,
                'book_title' => $book_title,
            ),
            'message' => $comment_message,
            'create_dt' => $commnet_create_dt,
        );

        $comments[] = $comment;
    }

    return $comments;
}
