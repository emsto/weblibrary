<?php

define('VALIDATION_OK', true);
define('VALIDATION_LENGTH_TOOSHORT', 'Value too short, expected at least %d chars');
define('VALIDATION_LENGTH_TOOLONG', 'Value too long, expected maximum %d chars');

/**
 * 
 * @param string $str
 * @param int $min
 * @param int $max
 * @return int
 */
function validate_length($str, $min, $max)
{
    if (mb_strlen($str) < $min) {
        return sprintf(VALIDATION_LENGTH_TOOSHORT, $min);
    }

    if (mb_strlen($str) > $max) {
        return sprintf(VALIDATION_LENGTH_TOOLONG, $max);
    }

    return VALIDATION_OK;
}
