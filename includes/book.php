<?php

require_once 'config.php';
require_once 'db.php';

/**
 * 
 * @param array $data
 * @return int
 */
function book_create(array $data)
{
    $sql = 'INSERT INTO ' . DB_TABLE_BOOKS . ' (book_title) VALUES (?)';

    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, $sql);

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_bind_param($stmt, 's', $data['book_title']);
    mysqli_execute($stmt);

    $book_id = mysqli_stmt_insert_id($stmt);

    if ($book_id && isset($data['authors']) && is_array($data['authors'])) {

        $sql = 'INSERT INTO ' . DB_TABLE_BOOKS_AUTHORS . '(book_id, author_id) VALUES (?, ?)';

        $stmt = mysqli_prepare($link, $sql);

        if (!$stmt) {
            trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
        }

        foreach ($data['authors'] as $author) {
            mysqli_stmt_bind_param($stmt, 'ii', $book_id, $author['author_id']);
            mysqli_stmt_execute($stmt);
        }
    }

    return $book_id;
}

/**
 * 
 * @param int $book_id
 * @param array $data
 * @return boolean
 */
function book_update($id, array $data)
{
    $book_id = (int) $id;

    $sql = 'UPDATE ' . DB_TABLE_BOOKS . ' SET book_title = ? WHERE book_id = ?';

    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, $sql);

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_bind_param($stmt, 'si', $data['book_title'], $book_id);
    mysqli_execute($stmt);

    $author_ids = array();

    if (isset($data['authors']) && is_array($data['authors'])) {
        foreach ($data['authors'] as $author) {
            array_push($author_ids, (int) $author['author_id']);
        }
    }

    $sql = 'DELETE FROM ' . DB_TABLE_BOOKS_AUTHORS . ' WHERE book_id = ? AND author_id NOT IN(' . implode(', ', $author_ids) . ')';

    if (!($stmt = mysqli_prepare($link, $sql))) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_bind_param($stmt, 'i', $book_id);
    mysqli_stmt_execute($stmt);

//    $sql = 'INSERT INTO ' . DB_TABLE_BOOKS_AUTHORS . ' (book_id, author_id) SELECT * FROM (SELECT ? AS book_id, ? AS author_id) AS tmp WHERE NOT EXISTS (SELECT book_id, author_id FROM ' . DB_TABLE_BOOKS_AUTHORS . ' WHERE book_id = ? AND author_id = ?) LIMIT 1';
//    
//    if (!($stmt = mysqli_prepare($link, $sql))) {
//        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
//    }

    foreach ($author_ids as $author_id) {
        //mysqli_stmt_bind_param($stmt, 'iiii', $book_id, $author_id, $book_id, $author_id);
        $sql = 'INSERT INTO ' . DB_TABLE_BOOKS_AUTHORS . ' (book_id, author_id) SELECT * FROM (SELECT %d AS book_id, %d AS author_id) AS tmp WHERE NOT EXISTS (SELECT book_id, author_id FROM ' . DB_TABLE_BOOKS_AUTHORS . ' WHERE book_id = %d AND author_id = %d) LIMIT 1';
        $query = sprintf($sql, $book_id, $author_id, $book_id, $author_id);
        mysqli_query($link, $query);
    }

    return true;
}

/**
 * 
 * @param int $book_id
 * @return bool
 */
function book_delete($book_id)
{
    $sql = 'DELETE FROM ' . DB_TABLE_BOOKS . ' WHERE book_id = ?';
    $sql2 = 'DELETE FROM ' . DB_TABLE_BOOKS_AUTHORS . ' WHERE book_id = ?';

    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, $sql);
    $stmt2 = mysqli_prepare($link, $sql2);

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    if (!$stmt2) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_bind_param($stmt, 'i', $book_id);
    mysqli_stmt_bind_param($stmt2, 'i', $book_id);

    mysqli_stmt_execute($stmt);
    mysqli_stmt_execute($stmt2);

    return true;
}

/**
 * 
 * @return type
 */
function book_load($book_id)
{
    $sql = 'SELECT b.book_id, b.book_title, a.author_id, a.author_name FROM ' . DB_TABLE_BOOKS . ' AS b ';
    $sql .= 'LEFT JOIN ' . DB_TABLE_BOOKS_AUTHORS . ' AS ba ON b.book_id = ba.book_id ';
    $sql .= 'LEFT JOIN ' . DB_TABLE_AUTHORS . ' AS a ON a.author_id = ba.author_id ';
    $sql .= 'WHERE b.book_id = ?';

    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, $sql);

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_bind_param($stmt, 'i', $book_id);
    mysqli_execute($stmt);

    mysqli_stmt_bind_result($stmt, $book_id, $book_title, $author_id, $author_name);

    $book = array();
    while (mysqli_stmt_fetch($stmt)) {
        $book['book_id'] = $book_id;
        $book['book_title'] = $book_title;
        if (!isset($book['authors'])) {
            $book['authors'] = array();
        }
        if ($author_id) {
            $book['authors'][$author_id] = array(
                'author_id' => $author_id,
                'author_name' => $author_name);
        }
    }

    if (empty($book)) {
        return false;
    }

    return $book;
}

/**
 * 
 * @param int|null $author_id
 * @return array
 */
function book_find_all($author_id = null)
{
    $sql = 'SELECT b.book_id, b.book_title, a.author_id, a.author_name FROM ' . DB_TABLE_BOOKS . ' AS b ';
    $sql .= 'LEFT JOIN ' . DB_TABLE_BOOKS_AUTHORS . ' AS ba ON b.book_id = ba.book_id ';
    $sql .= 'LEFT JOIN ' . DB_TABLE_AUTHORS . ' AS a ON a.author_id = ba.author_id ';

    if ($author_id) {
        $sql .= 'LEFT JOIN ' . DB_TABLE_BOOKS_AUTHORS . ' AS xba ON b.book_id = xba.book_id ';
        $sql .= 'LEFT JOIN ' . DB_TABLE_AUTHORS . ' AS xa ON xa.author_id = xba.author_id ';
        $sql .= 'WHERE xa.author_id = ' . (int) $author_id;
    }

    $sql = 'SELECT s.*, COUNT(c.id) FROM (' . $sql . ') AS s ';
    $sql .= 'LEFT JOIN ' . DB_TABLE_COMMENTS . ' AS c ON c.book_id = s.book_id ';
    $sql .= 'GROUP BY s.book_id, s.author_id';
    
    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, $sql);

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_execute($stmt);

    $comments_count = -1;
    mysqli_stmt_bind_result($stmt, $book_id, $book_title, $author_id, $author_name, $comments_count);

    $books = array();
    while (mysqli_stmt_fetch($stmt)) {
        $books[$book_id]['book_id'] = $book_id;
        $books[$book_id]['book_title'] = $book_title;
        if (!isset($books[$book_id]['authors'])) {
            $books[$book_id]['authors'] = array();
        }
        if ($author_id) {
            $books[$book_id]['authors'][$author_id] = array(
                'author_id' => $author_id,
                'author_name' => $author_name);
        }
        $books[$book_id]['comments_count'] = $comments_count;
    }

    return $books;
}

/**
 * 
 * @param int $book_id
 * @return boolean
 */
function book_id_exists($book_id)
{
    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, 'SELECT author_id FROM ' . DB_TABLE_BOOKS . ' WHERE book_id = ?');

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_bind_param($stmt, 'i', $book_id);
    mysqli_stmt_execute($stmt);

    while (mysqli_stmt_fetch($stmt)) {
        return true;
    }

    return false;
}

/**
 * 
 * @param string $book_title
 * @return boolean
 */
function book_title_exists($book_title, $exclude_id = 0)
{
    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, 'SELECT book_id FROM ' . DB_TABLE_BOOKS . ' WHERE LOWER(book_title) = ? AND book_id != ?');

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    $book_title = mb_strtolower($book_title);
    mysqli_stmt_bind_param($stmt, 'si', $book_title, $exclude_id);
    mysqli_stmt_execute($stmt);

    while (mysqli_stmt_fetch($stmt)) {
        return true;
    }

    return false;
}
