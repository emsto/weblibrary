<?php

defined('SESSION_NOTIFY_ERRORS') || define('SESSION_NOTIFY_ERRORS', 'N_ERRORS');
defined('SESSION_NOTIFY_SUCCESS') || define('SESSION_NOTIFY_SUCCESS', 'N_SUCCESS');
defined('SESSION_USER_ID') || define('SESSION_USER_ID', 'user_id');
defined('SESSION_USER_TYPE') || define('SESSION_USER_TYPE', 'user_type');
defined('SESSION_USER_USERNAME') || define('SESSION_USER_USERNAME', 'user_username');

session_start();

/**
 * 
 * @return bool
 */
function session_has_user()
{
    return isset($_SESSION[SESSION_USER_ID]) && $_SESSION[SESSION_USER_ID];
}

/**
 * 
 * @return int
 */
function session_get_user_id()
{
    if (!session_has_user()) {
        trigger_error('Current session does not have a valid user.', E_USER_ERROR);
    }

    return $_SESSION[SESSION_USER_ID];
}

/**
 * 
 * @return int
 */
function session_get_user_type()
{
    if (!session_has_user()) {
        trigger_error('Current session does not have a valid user.', E_USER_ERROR);
    }

    return $_SESSION[SESSION_USER_TYPE];
}

/**
 * 
 * @return string
 */
function session_get_user_username()
{
    if (!session_has_user()) {
        trigger_error('Current session does not have a valid user.', E_USER_ERROR);
    }

    return $_SESSION[SESSION_USER_USERNAME];
}

/**
 * 
 * @param int $id
 * @param string $username
 */
function session_set_user($id, $type, $username)
{
    $_SESSION[SESSION_USER_ID] = (int) $id;
    $_SESSION[SESSION_USER_TYPE] = (int) $type;
    $_SESSION[SESSION_USER_USERNAME] = (string) $username;
}

/**
 * 
 * @param array|string $messages
 */
function session_add_error_messages($messages)
{
    $messages = (array) $messages;

    if (isset($_SESSION[SESSION_NOTIFY_ERRORS])) {
        $messages = array_merge((array) $_SESSION[SESSION_NOTIFY_ERRORS], $messages);
    }

    $_SESSION[SESSION_NOTIFY_ERRORS] = $messages;
}

/**
 * 
 * @param bool $clear
 * @return array
 */
function session_get_error_messages($clear = false)
{
    $messages = isset($_SESSION[SESSION_NOTIFY_ERRORS]) ? (array) $_SESSION[SESSION_NOTIFY_ERRORS] : array();

    if ($clear) {
        unset($_SESSION[SESSION_NOTIFY_ERRORS]);
    }

    return $messages;
}

/**
 * 
 * @param array|string $message
 */
function session_add_success_messages($messages)
{
    $messages = (array) $messages;

    if (isset($_SESSION[SESSION_NOTIFY_SUCCESS])) {
        $messages = array_merge((array) $_SESSION[SESSION_NOTIFY_SUCCESS], $messages);
    }

    $_SESSION[SESSION_NOTIFY_SUCCESS] = $messages;
}

/**
 * 
 * @param bool $clear
 * @return array
 */
function session_get_success_messages($clear = false)
{
    $messages = isset($_SESSION[SESSION_NOTIFY_SUCCESS]) ? (array) $_SESSION[SESSION_NOTIFY_SUCCESS] : array();

    if ($clear) {
        unset($_SESSION[SESSION_NOTIFY_SUCCESS]);
    }

    return $messages;
}

$no_session_scripts = array('login.php', 'register.php');
$current_script = basename($_SERVER['SCRIPT_NAME']);

if (session_has_user() && in_array($current_script, $no_session_scripts)) {
    header('Location: ' . APPLICATION_BASE_URL . '/index.php');
    exit;
}
