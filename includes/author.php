<?php

require_once 'config.php';
require_once 'db.php';

/**
 * 
 * @param array $data
 * @return boolean
 */
function author_create(array $data)
{
    $sql = 'INSERT INTO ' . DB_TABLE_AUTHORS . ' (author_name) VALUES (?)';

    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, $sql);

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_bind_param($stmt, 's', $data['author_name']);
    mysqli_execute($stmt);

    return mysqli_stmt_affected_rows($stmt) > 0;
}

/**
 * 
 * @param int $id
 * @param array $data
 * @return boolean
 */
function author_update($id, array $data)
{
    $id = (int) $id;

    $sql = 'UPDATE ' . DB_TABLE_AUTHORS . ' SET author_name = ? WHERE author_id = ?';

    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, $sql);

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_bind_param($stmt, 'si', $data['author_name'], $id);
    mysqli_execute($stmt);

    return true;
}

/**
 * 
 * @param int $id
 * @return bool
 */
function author_delete($id)
{
    $sql = 'DELETE FROM ' . DB_TABLE_AUTHORS . ' WHERE author_id = ?';
    $sql2 = 'DELETE FROM ' . DB_TABLE_BOOKS_AUTHORS . ' WHERE author_id = ?';

    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, $sql);
    $stmt2 = mysqli_prepare($link, $sql2);

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }
    
    if (!$stmt2) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_bind_param($stmt, 'i', $id);
    mysqli_stmt_bind_param($stmt2, 'i', $id);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_execute($stmt2);

    return mysqli_stmt_affected_rows($stmt) > 0;
}

/**
 * 
 * @param int $id
 * @return array|false
 */
function author_load($id)
{
    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, 'SELECT author_id, author_name FROM ' . DB_TABLE_AUTHORS . ' WHERE author_id = ?');

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_bind_param($stmt, 'i', $id);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $author_id, $author_name);

    if (!mysqli_stmt_fetch($stmt)) {
        return false;
    }

    $author = array(
        'author_id' => $author_id,
        'author_name' => $author_name,
    );

    return $author;
}

/**
 * 
 * @param int $id
 * @return boolean
 */
function author_id_exists($id)
{
    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, 'SELECT author_id FROM ' . DB_TABLE_AUTHORS . ' WHERE author_id = ?');

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_bind_param($stmt, 'i', $id);
    mysqli_stmt_execute($stmt);

    while (mysqli_stmt_fetch($stmt)) {
        return true;
    }

    return false;
}

/**
 * 
 * @param string $author_name
 * @return boolean
 */
function author_name_exists($author_name, $exclude_id = 0)
{
    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, 'SELECT author_id FROM ' . DB_TABLE_AUTHORS . ' WHERE LOWER(author_name) = ? AND author_id != ?');

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    $author_name = mb_strtolower($author_name);
    mysqli_stmt_bind_param($stmt, 'si', $author_name, $exclude_id);
    mysqli_stmt_execute($stmt);

    while (mysqli_stmt_fetch($stmt)) {
        return true;
    }

    return false;
}

/**
 * 
 * @return array
 */
function author_find_all()
{
    $sql = 'SELECT * FROM `' . DB_TABLE_AUTHORS . '`';

    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, $sql);

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_execute($stmt);

    mysqli_stmt_bind_result($stmt, $author_id, $author_name);

    $authors = array();
    while (mysqli_stmt_fetch($stmt)) {
        $authors[$author_id] = array(
            'author_id' => $author_id,
            'author_name' => $author_name,
        );
    }

    return $authors;
}
