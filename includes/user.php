<?php

require_once 'db.php';

define('USER_TYPE_ADMIN', 1);
define('USER_TYPE_USER', 0);

/**
 * 
 * @param string $identity
 * @param string $credential
 * @return array|null
 */
function user_authenticate($identity, $credential)
{
    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, 'SELECT id, type, username FROM ' . DB_TABLE_USERS . ' WHERE username = ? AND password = PASSWORD(?)');

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_bind_param($stmt, 'ss', $identity, $credential);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $user_id, $user_type, $user_username);

    if (mysqli_stmt_fetch($stmt)) {
        return array('id' => $user_id, 'type' => $user_type, 'username' => $user_username);
    }

    return false;
}

/**
 * 
 * @param array $data
 */
function user_create($data)
{
    $sql = 'INSERT INTO ' . DB_TABLE_USERS . '(type, username, password) VALUES (?, ?, PASSWORD(?))';

    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, $sql);

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_bind_param($stmt, 'iss', $data['type'], $data['username'], $data['password']);
    mysqli_stmt_execute($stmt);

    return mysqli_stmt_affected_rows($stmt) > 0;
}

/**
 * 
 * @param int $user_id
 * @return array|null
 */
function user_load($user_id)
{
    $user_id = (int) $user_id;

    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, 'SELECT id, type, username FROM ' . DB_TABLE_USERS . ' WHERE id = ?');

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    mysqli_stmt_bind_param($stmt, 'i', $user_id);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $user_id, $user_type, $user_username);

    if (!mysqli_stmt_fetch($stmt)) {
        return false;
    }

    $user = array(
        'id' => $user_id,
        'type' => $user_type,
        'username' => $user_username,
    );

    return $user;
}

/**
 * 
 * @param int $id
 * @return boolean
 */
function user_username_exists($username)
{
    $link = mysqli_get_link();
    $stmt = mysqli_prepare($link, 'SELECT id FROM ' . DB_TABLE_USERS . ' WHERE LOWER(username) = ?');

    if (!$stmt) {
        trigger_error('Cannot prepare ' . __FUNCTION__ . ' sql statement', E_USER_ERROR);
    }

    $username = strtolower($username);
    mysqli_stmt_bind_param($stmt, 's', $username);
    mysqli_stmt_execute($stmt);
    while (mysqli_stmt_fetch($stmt)) {
        return true;
    }

    return false;
}
