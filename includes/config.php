<?php

error_reporting(E_ALL);
ini_set('display_errors ', true);
ini_set('display_startup_errors ', true);

define('APPLICATION_NAME', 'WebLibrary');
define('APPLICATION_VERSION', '0.0.5');

$document_root = str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']);
$dirname = str_replace('\\', '/', dirname(__FILE__));
$baseurl = substr($dirname, strlen($document_root));
$baseurl = substr($baseurl, 0, strrpos($baseurl, '/'));

define('APPLICATION_BASE_URL', $baseurl);
define('APPLICATION_CONTROLLERS_URL', APPLICATION_BASE_URL . '/app');
define('APPLICATION_ROOT', dirname(dirname(__FILE__)));

define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'books');

define('DB_TABLE_AUTHORS', 'authors');
define('DB_TABLE_BOOKS', 'books');
define('DB_TABLE_BOOKS_AUTHORS', 'books_authors');
define('DB_TABLE_COMMENTS', 'comments');
define('DB_TABLE_USERS', 'users');

