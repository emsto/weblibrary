<?php
require_once dirname(__FILE__) . '/includes/config.php';
require_once APPLICATION_ROOT . '/includes/session.php';
require_once APPLICATION_ROOT . '/includes/user.php';

$displayError = false;

if ($_POST) {
    $user = user_authenticate($_POST['username'], $_POST['password']);
    if ($user) {
        session_regenerate_id(true);
        call_user_func_array('session_set_user', $user);
        header('Location: ' . APPLICATION_BASE_URL . '/index.php');
        exit;
    }

    $displayError = true;
}

include APPLICATION_ROOT . '/phtml/header.php';
?>

<div class="row">
    <div class="panel panel-default col-md-4 col-md-offset-4">
        <div class="panel-body">
            <h3>Login</h3>
            <hr />
            <?php if ($displayError): ?>
                <div class="alert alert-danger">
                    Invalid username and/or password.
                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                </div>
            <?php endif; ?>
            <form role="form" method="post">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username" placeholder="Enter username">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-primary">Login</button>
                or <a href="register.php">Register</a>
            </form>
        </div>
    </div>
</div>

<?php
include APPLICATION_ROOT . '/phtml/footer.php';
